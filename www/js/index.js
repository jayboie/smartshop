/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var appp = {
    // Application Constructor
    initialize: function() {
         //localStorage.setItem('deviceimei',reg_id);
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
       // alert("push1");
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
       // alert("push2");
        try {
        var push = PushNotification.init({
            "android": {
                "senderID": "703630268544"
            },
            "ios": {"alert": "true", "badge": "true", "sound": "true"}, 
            "windows": {} 
        });
        
        push.on('registration', function(data) {
            alert("registration event");
             
            reg_id=data.registrationId;

            if(typeof(localStorage.deviceimei)=="undefined"){
                registerDevice();
            }
            
             //window.clipboardData.setData("Text", data.registrationId);
            //alert(JSON.stringify(data));
        });

        push.on('notification', function(data) {
        	console.log("notification event");
            alert(JSON.stringify(data.message));
              
            push.finish(function () {
               // alert('finish successfully called');
            });
        });

        push.on('error', function(e) {
           // alert("push error");
        });
    }

    catch(err) {
  //alert(err.message);
}

}
};

reg_id='';
appp.initialize();


//ROOT="http://localhost/mobileapps/cordovapush/hello/dashboard/push/example.php";
//ROOT="http://localhost/mobileapps/emergency/dashboard/admin/sendPush";
ROOT="http://www.gsffuta.com/dashboard/admin/";

function registerDevice () {

    localStorage.setItem("deviceimei",reg_id);
     //alert("");
    var request = $.ajax({
        url: ROOT+'/registerDevice',
        type: "POST",
        data: { 
            regid:reg_id
         }
  
});

  
request.done(function( msg ) { 
 //alert(jQuery.parseJSON(msg));

 localStorage.setItem("registered","1");
  
});

request.error(function( msg ) { 
   // alert(jQuery.parseJSON(msg));
});
  
}
 