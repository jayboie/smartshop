(function(){
  'use strict';
  var module = angular.module('app', ['onsen','angularUtils.directives.dirPagination']); 

  module.run(['$rootScope', function ($rootScope) {
  $rootScope.APIROOT = "http://localhost/mobileapps/Template/SmartShop/dashboard/admin/";
  // $rootScope.APIROOT = "http://osmotechs.com/SmartShop/dashboard/admin/";
    $rootScope.pageTitle='';
    $rootScope.user = {
      type: '',
      username:'',
      password:''
    }
    $rootScope.signup={}
    $rootScope.newproduct={}
    $rootScope.currentMerchant={}
    $rootScope.support={}
    $rootScope.customerInfo={}
    $rootScope.paymentStatus='paylater';
    $rootScope.merchantInfo={}
    $rootScope.merchants_db=[];
    $rootScope.wallet={}
    $rootScope.addProduct={}
    $rootScope.products={
      merchantProducts:[],
      cart:[],
      filter:"All"
    };
    $rootScope.adverts=[{title:"Black Friday", description: "get your dream laptop for 5% off, order with smartshop, offer closes Feb 28",merchant:'Jumia'},
                        {title:"Coffe for Two", description:"Buy a coffee and get one for free at Cafe Neo",merchant:'Cafe Neo'}
                        ]

  }]);

  module.controller('SmartShopCtrl', ['$scope', '$http', '$rootScope', function($scope, $http, $rootScope){

      var api =  $rootScope.APIROOT; 
      /*$http.get(api +'allMerchants/').success(function (res) {
        $rootScope.merchants_db = res;
        console.log(res);

      }).error(function(err) {
        /* Act on the event */
     /*   console.log("yadadad, its didnt work! "+ err);
      });*/

      $scope.chooseUserType = function (type) {
        $rootScope.user = $scope.user;
        localStorage.setItem('AccountType',type);
      }

      $scope.loginDetails = function () {
        $rootScope.user.username = $scope.user.username;
        console.log($rootScope.user.username);
      }

      $scope.pushPage = function (page) {
        menu.setMainPage(page);
        menu.closeMenu();
      }

      $scope.signupCustomer=function(){
        $scope.data=$rootScope.signup;
        console.log($scope.data);
         $scope.data=$.param($scope.data);
        api=$rootScope.APIROOT+'/registerCustomerApi'
        $http({method: 'post', url: api,headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
              data: $scope.data}).
        then(function(response) {
             console.log(response);
             if(response.data!="Success"){
              showAlert(response.data);
             }else{
              showAlert("An account has being created for you");
               main_navigator.pushPage('login.html', {animation : 'none'}); 
             }
        }, function(response) {
          modal.hide();
            showAlert("Please check your internet connection");
        });
      }

       $scope.signupMerchant=function(){
        $scope.data=$rootScope.signup;
        console.log($scope.data);
         $scope.data=$.param($scope.data);
        api=$rootScope.APIROOT+'/registerMerchantApi'
        $http({method: 'post', url: api,headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
              data: $scope.data}).
        then(function(response) {
             console.log(response);
             if(response.data!="Success"){
              showAlert(response.data);
             }else{
              showAlert("An account has being created for you");
               main_navigator.pushPage('login.html', {animation : 'none'}); 
             }
        }, function(response) {
          modal.hide();
            showAlert("Please check your internet connection");
        });
      }

      

      $scope.showSupport = function () {
          $rootScope.pageTitle='SUPPORT';
          main_navigator.pushPage('support.html', {animation : 'none'}); 
          menu.closeMenu();
      }

      $scope.showMessage = function () {
          $rootScope.pageTitle='MESSAGING';
          $scope.data={
            id:$rootScope.merchantInfo.merchantInfo[0].id
          } 

           $scope.data=$.param($scope.data);
        api=$rootScope.APIROOT+'/pullMerchantMessages'
        $http({method: 'post', url: api,headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
              data: $scope.data}).
        then(function(response) {
             console.log(response);
             $rootScope.merchantInfo.messages=response.data
        }, function(response) {
          modal.hide();
            showAlert("Please check your internet connection");
        });
          main_navigator.pushPage('messaging.html', {animation : 'none'}); 
          menu.closeMenu();
      }
      $scope.messageInfo=function(message){
        $scope.message=message;
        main_navigator.pushPage('messageInfo.html', {animation : 'slide'}); 
      }

      $scope.showAdvert = function () {
          $rootScope.pageTitle='ADVERTS';
          main_navigator.pushPage('advert.html', {animation : 'none'}); 
          menu.closeMenu();
      }

      $scope.sendBill = function () {
          $rootScope.pageTitle='SEND BILL';
          main_navigator.pushPage('bill.html', {animation : 'none'}); 
          menu.closeMenu();
      }

       $scope.filterCustomerTransaction = function (type) {
          $rootScope.customerInfo.transactions.filter=type;
          $rootScope.customerInfo.filtered_transactions=[];
          for(var i=0; i<$rootScope.customerInfo.transactions.length; i++){
            if($rootScope.customerInfo.transactions[i].type==type){
              $rootScope.customerInfo.filtered_transactions.push($rootScope.customerInfo.transactions[i]);
            }
          }
          console.log($rootScope.customerInfo.filtered_transactions);
      }

       $scope.paymentRequest = function (product) {
          $rootScope.pageTitle='ADVERTS';
          modal.show();
          menu.closeMenu();
           $scope.data={     
          receiver:$rootScope.customerInfo.customerInfo[0].id
        }
        $scope.data=$.param($scope.data);
        api=$rootScope.APIROOT+'/pullTransactions'
        $http({method: 'post', url: api,headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
              data: $scope.data}).
        then(function(response) {
             console.log(response);
             $rootScope.customerInfo.transactions=response.data;
             $scope.filterCustomerTransaction("Bills");
             modal.hide();
             main_navigator.pushPage('paymentrequest.html', {animation : 'none'}); 
        }, function(response) {
          modal.hide();
            showAlert("Please check your internet connection");
        });
      }



      $scope.viewTransaction=function(transaction){
        console.log(transaction);
        $rootScope.products.cart=JSON.parse(transaction.description);
        console.log($rootScope.products.cart);
        $scope.calcTotal();
        $scope.transaction=transaction;
        main_navigator.pushPage('receipt.html', {animation : 'slide'});
      }

      

      $scope.pullMessages = function () {
          $scope.chatmessage='';
          console.log($rootScope.currentMerchant);
          $rootScope.pageTitle='MESSAGES';
          modal.show();
          menu.closeMenu();
           $scope.data={     
              customerid:$rootScope.customerInfo.customerInfo[0].id,
              merchantid:$rootScope.currentMerchant.id
            }
        $scope.data=$.param($scope.data);
        api=$rootScope.APIROOT+'/pullMessages'
        $http({method: 'post', url: api,headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
              data: $scope.data}).
        then(function(response) {
          modal.hide();
             console.log(response);
              $scope.messages=response.data;
             main_navigator.pushPage('chat.html', {title : 'josh'});
        }, function(response) {
          modal.hide();
            showAlert("Please check your internet connection");
        });
      }

       $scope.sendSupport=function(){
        $scope.data=$rootScope.support;
        $scope.data.usertype=$rootScope.user.type;
        $scope.data=$.param($scope.data);
         api=$rootScope.APIROOT+'/sendSupport'; 
         modal.show();
        $http({method: 'post', url: api,headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
              data: $scope.data}).
        then(function(response) {
            modal.hide(); 
            if(response.data.response=="Success"){
              showAlert("Request sent, Support team will get back to you shortly");
            }else{
              showAlert(response.data.response);
            }
        }, function(response) {
            modal.hide();
            showAlert("Please check your internet connection");
        });
         
      }

      $scope.sendMessage = function () {
          $rootScope.pageTitle='MESSAGES';
          modal.show();
          menu.closeMenu();
          $scope.data=$rootScope.support;
           $scope.data.customerid=$rootScope.customerInfo.customerInfo[0].id;
           $scope.data.merchantid=$rootScope.currentMerchant.id; 
           console.log($scope.data);
        $scope.data=$.param($scope.data);
        api=$rootScope.APIROOT+'/sendMessage'
        $http({method: 'post', url: api,headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
              data: $scope.data}).
        then(function(response) {
          modal.hide();
             if(response.data.response=="Success"){

             }else{
              showAlert(response.data.response);
             }
        }, function(response) {
          modal.hide();
            showAlert("Please check your internet connection");
        });
      }

      $scope.pullHistory = function (product) {
          $rootScope.pageTitle='HISTORY';
          modal.show();
          menu.closeMenu();
          $scope.data={     
          sender:$rootScope.merchantInfo.merchantInfo[0].id
          }
        $scope.data=$.param($scope.data);
        api=$rootScope.APIROOT+'/pullMerchantTransactions'
        $http({method: 'post', url: api,headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
              data: $scope.data}).
        then(function(response) {
             console.log(response);
             $rootScope.merchantInfo.transactions=response.data
             $rootScope.merchantInfo.transactions.filter="Bills";
             $rootScope.merchantInfo.transactions.typestatus="success";
             $scope.filterTransaction("Bills");
             modal.hide();
             //$scope.showHistory();
            
             main_navigator.pushPage('historyfilter.html', {animation : 'none'}); 
              setTimeout($scope.transactionGraph,500);
        }, function(response) {
          modal.hide();
            showAlert("Please check your internet connection");
        });
      }

      $scope.showHistory=function(){
        //alert('');
        //$rootScope.merchantInfo=JSON.parse(localStorage.merchantInfo);
        $scope.pullHistory();
        console.log($rootScope.merchantInfo);
        // main_navigator.pushPage('historyfilter.html', {animation : 'slide'});
         menu.closeMenu();
      }

       $scope.filterTransaction = function (type) {
          $scope.total_amount=0;
          $rootScope.merchantInfo.transactions.filter=type;
          $rootScope.merchantInfo.filtered_transactions=[];
          for(var i=0; i<$rootScope.merchantInfo.transactions.length; i++){
            console.log($rootScope.merchantInfo.transactions.typestatus+" "+$rootScope.merchantInfo.transactions.typestatus); 
             if($rootScope.merchantInfo.transactions[i].status=="bookednotpaid"){
                $rootScope.merchantInfo.transactions[i].status='pending';
            }
            if($rootScope.merchantInfo.transactions[i].status=="bookedpaid"){
                $rootScope.merchantInfo.transactions[i].status='success';
            }
            if(($rootScope.merchantInfo.transactions[i].type==type) && ($rootScope.merchantInfo.transactions[i].status==$rootScope.merchantInfo.transactions.typestatus)){
              $rootScope.merchantInfo.filtered_transactions.push($rootScope.merchantInfo.transactions[i]);
              var invoice=JSON.parse($rootScope.merchantInfo.transactions[i].description);
              for(var j=0; j< invoice.length; j++){
                $scope.total_amount+=Number(invoice[j].price);
              }
             // +=Number($rootScope.merchantInfo.transactions[i].amount);
            } 
          }
          $scope.total_amount=$scope.formatToNaira($scope.total_amount);
          console.log($rootScope.merchantInfo.filtered_transactions);
      }

      $scope.transactionGraph=function(){
        var success=0; var cancelled=0;
        var pending=0; var failed=0;
    for(var i=0; i<$rootScope.merchantInfo.transactions.length; i++){
             if($rootScope.merchantInfo.transactions[i].status=="bookednotpaid"){
                $rootScope.merchantInfo.transactions[i].status='pending';
            }
            if($rootScope.merchantInfo.transactions[i].status=="bookedpaid"){
                $rootScope.merchantInfo.transactions[i].status='success';
            }
            if(($rootScope.merchantInfo.transactions[i].status=='success')){
             success+=1;
            } 
            if(($rootScope.merchantInfo.transactions[i].status=='pending')){
             pending+=1;
            } 
        }

    $('#containert').highcharts({
        
        chart: {
            type: 'column',
            spacingBottom: 30
        },
        title: {
            text: 'Transaction History'
        },
        subtitle: {
            text: '',
            floating: true,
            align: 'right',
            verticalAlign: 'bottom',
            y: 15
        },
       
        xAxis: {
            categories: ['success','pending','failed','cancelled']
        },
        yAxis: {
            title: {
                text: 'Y-Axis'
            },
            labels: {
                formatter: function () {
                    return this.value;
                }
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    this.x + ': ' + this.y;
            }
        },
        plotOptions: {
            area: {
                fillOpacity: 0.5
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Number of Transactions',
            data: [success,pending,0,0]
        } ]
    });
 
 
      }

      $scope.transactionType=function(type){
         $rootScope.merchantInfo.transactions.typestatus=type;
         main_navigator.pushPage('history.html', {animation : 'slide'});
         $scope.filterTransaction('Bills');
      }

       $scope.saveProduct=function(){
        $rootScope.addProduct=$scope.newproduct;
        $scope.data=$rootScope.addProduct;
        $scope.data.merchantid=$rootScope.merchantInfo.merchantInfo[0].id;
        $scope.data.discount_type="Percentage";
        console.log($scope.data);
        $scope.data=$.param($scope.data);
         api=$rootScope.APIROOT+'/saveProduct'; 
         modal.show();
        $http({method: 'post', url: api,headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
              data: $scope.data}).
        then(function(response) {
            modal.hide(); 
            $scope.merchantStore();
        }, function(response) {
            modal.hide();
            showAlert("Please check your internet connection");
        });
         
      }

      $scope.merchantStore = function () {
          $rootScope.pageTitle=$rootScope.merchantInfo.merchantInfo[0].companyname;
          $scope.products.filter="All";
          $rootScope.products.cart=[];
          modal.show();
          $scope.data={     
             id:$rootScope.merchantInfo.merchantInfo[0].id
          }
          $scope.data=$.param($scope.data);
          api=$rootScope.APIROOT+'/pullMerchantProducts'
          $http({method: 'post', url: api,headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
              data: $scope.data}).
          then(function(response) {
             console.log(response);
             $rootScope.merchantInfo.products=response.data
             modal.hide();
        }, function(response) {
          modal.hide();
            showAlert("Please check your internet connection");
        });
          main_navigator.pushPage('merchantStore.html', {animation : 'slide'});
          menu.closeMenu();
      }

      $scope.productInformation = function (product) {
          $scope.product=product;
          $rootScope.pageTitle=$rootScope.merchantInfo.merchantInfo[0].companyname;
          $scope.products.filter="All";
          main_navigator.pushPage('productInformation.html', {animation : 'slide'});
          
      }

      $scope.deleteProduct=function(product){
          $scope.data={
            id:product.id,
            merchantid:$rootScope.merchantInfo.merchantInfo[0].id
          }
          $scope.data=$.param($scope.data);
          api=$rootScope.APIROOT+'/deleteMerchantProduct'
          $http({method: 'post', url: api,headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
              data: $scope.data}).
          then(function(response) {
             console.log(response);
             $rootScope.merchantInfo.products=response.data;
             showAlert("Product deleted");
             main_navigator.pushPage('merchantStore.html', {animation : 'slide'});
          }, function(response) {
            showAlert("Please check your internet connection");
        });
      }

      $scope.updateProduct=function(product){
          $scope.data={
            id:product.id,
            price:product.price,
            discount_amount:product.discount_amount,
            merchantid:$rootScope.merchantInfo.merchantInfo[0].id,
            name:product.name
          }
          $scope.data=$.param($scope.data);
          api=$rootScope.APIROOT+'/updateMerchantProduct'
          $http({method: 'post', url: api,headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
              data: $scope.data}).
          then(function(response) {
             console.log(response);
             $rootScope.merchantInfo.products=response.data;
             showAlert("Product Updated");
             main_navigator.pushPage('merchantStore.html', {animation : 'slide'});
          }, function(response) {
            showAlert("Please check your internet connection");
        });
      }

       $scope.merchantsProduct = function (merchant) {
          $rootScope.pageTitle=merchant.companyname;
          $rootScope.currentMerchant=merchant;
          $scope.totalprice=0;
          $scope.price=0;
          $scope.totaldiscount=0;
          $scope.products.filter="All";
          $rootScope.products.cart=[];
          console.log(merchant.id);
          $scope.data={     
           id:merchant.id
          }
          $scope.data=$.param($scope.data);
          api=$rootScope.APIROOT+'/pullMerchantProducts'
        $http({method: 'post', url: api,headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
              data: $scope.data}).
        then(function(response) {
             console.log(response);
              $rootScope.products.merchantProducts=response.data;
        }, function(response) {
            showAlert("Please check your internet connection");
        });
          main_navigator.pushPage('merchantProducts.html', {animation : 'slide'});
      }

      $scope.discountType = function (type) {
           $scope.discount_type=type;
      }

       $scope.addToCart = function (product) {
          $rootScope.products.cart.push(product);
          console.log($rootScope.products.cart);
          $scope.calcTotal(); 
          showAlert("Product added to cart");
      }
 
      $scope.removeProduct = function (product) {
       $scope.productIndex=$rootScope.products.cart.indexOf(product);
       $scope.temp=[];
       for (var i=0; i < $rootScope.products.cart.length; i++) {
         if(i != $scope.productIndex){
          $scope.temp.push($rootScope.products.cart[i]);
         }
       };
       $rootScope.products.cart=$scope.temp;
       $scope.calcTotal();
      }

      $scope.calcTotal=function(){
        $scope.totalprice=0;
        $scope.totaldiscount=0;
        $scope.price=0;
          for(var i=0; i<$rootScope.products.cart.length; i++){
            $scope.totalprice +=  Number($rootScope.products.cart[i].price);
            $scope.price+=Number($rootScope.products.cart[i].price);
            if($rootScope.products.cart[i].discount_type=="Fixed Value"){
                $scope.totalprice-=Number($rootScope.products.cart[i].discount_amount);
                $scope.totaldiscount+=Number($rootScope.products.cart[i].discount_amount);
            }else{
              var discount_percentage=Number($rootScope.products.cart[i].price)*(Number($rootScope.products.cart[i].discount_amount)/100);
              $scope.totalprice-=Number(discount_percentage);
              $scope.totaldiscount+=Number(discount_percentage);
            }
          } 
      }

      $scope.paystatus=function(){
        if($rootScope.paymentStatus=='paylater'){
          $rootScope.paymentStatus='paynow';
        }else{
          $rootScope.paymentStatus='paylater';
        }
        console.log($rootScope.paymentStatus);
      }


      $scope.payWithSmartShop = function () {
        $scope.invoice=[];
        console.log($rootScope.currentMerchant);
        for (var i=0; i < $rootScope.products.cart.length; i++) {
          $scope.invoice.push({price:$rootScope.products.cart[i].price,itemname:$rootScope.products.cart[i].name,discount_amount:$rootScope.products.cart[i].discount_amount,discount_type:$rootScope.products.cart[i].discount_type});
       };
        $scope.invoice=JSON.stringify($scope.invoice); 
        var time = new Date();
        time=time.toString();
        $scope.data={     
          invoice:$scope.invoice,
          sender:$rootScope.currentMerchant.id,
          receiver:$rootScope.customerInfo.customerInfo[0].id,
          totalprice:$scope.totalprice,
          date:time,
          paymentStatus:$rootScope.paymentStatus
        }
        $scope.data=$.param($scope.data);
        api=$rootScope.APIROOT+'/bookItems';
        modal.show();
        $http({method: 'post', url: api,headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
              data: $scope.data}).
        then(function(response) {
             console.log(response);
             modal.hide();
              if(response.data.response!="Successfull"){
                showAlert(response.data.response);
              }else{
                showAlert("Items booked and paid successfully");
                $rootScope.customerInfo.wallet=response.data.walletinfo;
               $scope.viewTransaction(response.data.currenttrans[0]);
                console.log($rootScope.customerInfo.wallet);
              }
        }, function(response) {
          modal.hide();
            showAlert("Please check your internet connection");
        });
         
      }

      $scope.sendBillPay=function(){
        if(typeof($rootScope.merchantInfo.bill)=="undefined"){
          showAlert("Please Enter the SmartShop ID and Amount");
          return;
        }
        if((typeof($rootScope.merchantInfo.bill.amount)=="undefined") || (typeof($rootScope.merchantInfo.bill.receiver)=="undefined")){
           showAlert("Please Enter the SmartShop ID and Amount");
          return;
        } 
        console.log($rootScope.merchantInfo.bill); 
       $scope.invoice=[];
       var time = new Date();
        time=time.toString();
       $scope.invoice.push({price:$rootScope.merchantInfo.bill.amount,itemname:'Payment Request',discount_amount:'0',discount_type:'Fixed Value'});
        $scope.invoice=JSON.stringify($scope.invoice); 
        $scope.data=$rootScope.merchantInfo.bill;
        $scope.data.invoice=$scope.invoice;
        $scope.data.sender=$rootScope.merchantInfo.merchantInfo[0].id;
        $scope.data.totalprice=$rootScope.merchantInfo.bill.amount;
        $scope.data.date=time;
         $scope.data=$.param($scope.data);
        api=$rootScope.APIROOT+'/sendInvoice';
        modal.show();
         $http({method: 'post', url: api,headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
              data: $scope.data}).
        then(function(response) {
             console.log(response);
             modal.hide();
              if(response.data.response!="Successfull"){
                showAlert(response.data.response);
              }else{
                showAlert("Bill Sent successfully");
              }
        }, function(response) {
          modal.hide();
            showAlert("Please check your internet connection");
        });
      }

       $scope.filterProduct = function (type) {
          $rootScope.products.filter=type;
      }



     

      $scope.walletInfo = function () {
          $rootScope.pageTitle='WALLET';
          main_navigator.pushPage('user_wallet.html', {animation : 'slide'});
          menu.closeMenu();
      }

      $scope.recharge = function () {
        var walletSenderID='';
          if($rootScope.user.type=="merchant"){
            walletSenderID=$rootScope.merchantInfo.wallet[0].id;
           }else{
            walletSenderID=$rootScope.customerInfo.wallet[0].id;
           }
           $scope.data={
            walletSenderID:walletSenderID,
            amount:$rootScope.wallet.rechargeAmount
        }
        $scope.data=$.param($scope.data);
        modal.show();
        api=$rootScope.APIROOT+'/rechargewallet'; 
        $http({method: 'post', url: api,headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
              data: $scope.data}).
        then(function(response) {
            modal.hide();
             console.log(response);
             if($rootScope.user.type=="merchant"){
               $rootScope.merchantInfo.wallet[0]=response.data.Wallet[0];
              }else{
                $rootScope.customerInfo.wallet[0]=response.data.Wallet[0];
              }
             //$rootScope.customerInfo.wallet[0]=response.data.Wallet[0];
             showAlert("Wallet Recharged");
             $scope.walletInfo();
        }, function(response) {
            modal.hide();
            showAlert("Please check your internet connection");
        });
      }

      $scope.transfer = function (type) {
           console.log($rootScope.wallet.rechargeAmount);
           var walletSenderID='';
           if($rootScope.user.type=="merchant"){
            walletSenderID=$rootScope.merchantInfo.wallet[0].id;
           }else{
            walletSenderID=$rootScope.customerInfo.wallet[0].id;
           }
           $scope.data={
            walletSenderID:walletSenderID,
            amount:$rootScope.wallet.rechargeAmount,
            walletReceiverID:$rootScope.wallet.walletReceiverID,
        }
        $scope.data=$.param($scope.data);
        modal.show();
        api=$rootScope.APIROOT+'/transferCash'; 
        $http({method: 'post', url: api,headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
              data: $scope.data}).
        then(function(response) {
            modal.hide();
              if(response.data.response=="Success"){
                showAlert("Wallet Transfer Successfull"); 
                if($rootScope.user.type=="merchant"){
                    $rootScope.merchantInfo.wallet[0]=response.data.wallet[0]; 
                  }else{
                    $rootScope.customerInfo.wallet[0]=response.data.wallet[0]; 
                }
                $scope.walletInfo();
              }else{
                showAlert(response.data.response);
              }
        }, function(response) {
            modal.hide();
            showAlert("Please check your internet connection");
        });
      }

      $scope.showSignUp = function () {
        if($rootScope.user.type==""){
          showAlert("How do you want to use SmartShop");
          return;
        }
        main_navigator.pushPage('signup.html', {animation : 'lift'});
      }

       $scope.showLogin = function () {
        if($rootScope.user.type==""){
          showAlert("How do you want to use SmartShop");
          return;
        } 
        main_navigator.pushPage('login.html', {animation : 'lift'});
      }

      $scope.Login = function () {
        $rootScope.user= $scope.user;
        if($rootScope.user.type=="user"){
          $scope.loginCustomer();
        } else{
          $scope.loginMerchant();
        }    
      }

     

       $scope.loginCustomer = function () {
        modal.show();
        $scope.data={
            username:$rootScope.user.username,
            password:$rootScope.user.password
        }
        $scope.data=$.param($scope.data);
        api=$rootScope.APIROOT+'/loginCustomer'; 
        $http({method: 'post', url: api,headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
              data: $scope.data}).
        then(function(response) {
            modal.hide();
            if(response.data.response!="Success"){
              showAlert(response.data.response);
              return;
            }
            $rootScope.customerInfo=response.data;
            console.log($rootScope.customerInfo.customerInfo[0].fullname);
            $scope.profile=$rootScope.customerInfo.customerInfo[0];
            $scope.allMerchants();
        }, function(response) {
            modal.hide();
            showAlert("Please check your internet connection");
        });
      }

      $scope.allMerchants = function () {
          //$rootScope.customerInfo=JSON.parse(localStorage.customerInfo);
          console.log($rootScope.customerInfo);
          $scope.allAdvert='';
          for(var i=0; i < $rootScope.adverts.length; i++){
            $scope.allAdvert+=$rootScope.adverts[i].merchant+": "+$rootScope.adverts[i].title+": "+$rootScope.adverts[i].description;
          }
           var api =  $rootScope.APIROOT; 
            $http.get(api +'allMerchants/').success(function (res) {
              $rootScope.merchants_db = res;
              console.log(res);

            }).error(function(err) {
              /* Act on the event */
                console.log("yadadad, its didnt work! "+ err);
            });
          $rootScope.pageTitle='MERCHANTS';
          main_navigator.pushPage('customerDashboard.html', {animation : 'none'}); 
          menu.closeMenu();
      }

      $scope.loginMerchant = function () {
        modal.show();
           $scope.data={
            username:$rootScope.user.username,
            password:$rootScope.user.password
        }
        $scope.data=$.param($scope.data);
        api=$rootScope.APIROOT+'/loginMerchant'; 
        $http({method: 'post', url: api,headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
              data: $scope.data}).
        then(function(response) {
          modal.hide();
          if(response.data.response!="Success"){
              showAlert(response.data.response);
              return;
            }
            $rootScope.merchantInfo=response.data;
            console.log($rootScope.merchantInfo);
            $scope.pullHistory();
        }, function(response) {
          modal.hide();
            showAlert("Please check your internet connection");
        });
      }

      $scope.logout = function () {
          window.location.reload();
      }

      $scope.loadColors=function(){
        var colors=[];
        colors=["#e53935","#ff5252","#009688","#ff9100","#795548"];
        var index=Math.random() * (colors.length - 0) + 0;
        index=parseInt(index); 
        index=index % 5;
        console.log(index);
        return colors[index];
      }

      $scope.formatToNaira=function(num){
        var p = num.toFixed(2).split(".");
        return  p[0].split("").reverse().reduce(function(acc, num, i, orig) {
          return   num + (i && !(i % 3) ? "," : "") + acc;
        }, "") + "." + p[1];
      }

     

     

  }]);

    module.controller('CustomerCtrl', ['$scope', '$http', function($scope, $http){



    $scope.doDashboard = function () {


      $http.get(ROOT+'allMerchants/').success(function (res) {
        console.log(res);
      }).error(function(err) {
        /* Act on the event */
        console.log("yadadad, its didnt work! "+ err);
      });

    }
    
  }]);
 
})();

ROOT="http://localhost/mobileapps/Template/SmartShop/dashboard/admin/";
//ROOT="http://osmotechs.com/SmartShop/dashboard/admin/";
API=ROOT;

function init(){
  //ons.bootstrap();
  //main_navigator.pushPage('home.html', {title : 'josh'});
  
  setTimeout(function() {
        //sliders();
      }, 100);
}

sliderIndex=1;
function sliders(){
  sliderIndex+=1;
  if(sliderIndex==4){
    sliderIndex=0;
    carousel.first();
  }else{
    carousel.next();
  } 
  setTimeout(function() {
        sliders();
      }, 2000);
}

function login(){
  showAlert('Successfull');
  main_navigator.pushPage('customerDashboard.html', {animation : 'slide'});
  pullAllMerchants();

}

merchants_db=[];


function sendMessage(customerid,merchantid){
  modal.show();
  var request = $.ajax({
    url: API + 'allMerchants/',
    type: "POST",
    timeout:20000,
    datatype:'json',
    data: {  
      customerid:customerid,
      merchantid:merchantid
    } 
  }); 
  request.done(function(resp) {
    modal.hide(); 
    console.log(resp); 
    merchants_db=jQuery.parseJSON(resp);
    console.log(merchants_db); 
  }); 
  request.error(function( msg ) {
    modal.hide();
    showAlert("Oops! check internet connection"); 
  });
}

function saveProduct(){
  discount_type=$("#mydiscount_type").text();
  merchantid=$("#mymerchantid").text();
  productname=$("#myproductname").val();
  price=$("#myprice").val();
  discount_amt=$("#mydiscount_amt").val();
  console.log(merchantid+discount_type+discount_amt+productname+price);
  var request = $.ajax({
    url: API + 'saveProduct/',
    type: "POST",
    timeout:20000,
    datatype:'json',
    data: {  
      merchantid:merchantid,
      discount_type:discount_type,
      discount_amount:discount_amt,
      price:price,
      name:productname
    } 
  }); 
  request.done(function(resp) {
     showAlert("Product Successfully added");
     $("#storemenu").click();
  }); 
  request.error(function( msg ) {
    modal.hide();
    showAlert("Oops! check internet connection"); 
  });
}


 
function Login(){
  
  username=$("#user_name").val();
  password=$("#user_password").val();
  console.log(username+password);
  if((username=="") || (password=="")){
    showAlert("Sorry, you didnt enter all the details");
    return;
  }
  if(localStorage.AccountType=="merchant"){
    loginMerchant(username,password);
  }else{
    loginCustomer(username,password);
  }
  
}

function loginMerchant(username,password){
  modal.show();
  var request = $.ajax({
    url: API + 'loginMerchant/',
    type: "POST",
    timeout:20000,
    datatype:'json',
    data: {  
      username:username,
      password:password
    } 
  }); 
  request.done(function(resp) {
    modal.hide();
    localStorage.setItem('merchantInfo',resp);
    console.log(resp);
    resp=jQuery.parseJSON(resp);
    if(resp.response!="Success"){
      showAlert(resp.response);
      return;
    }else{
      $("#historymenu").click();
    }
   // localStorage.setItem("merchantInfo",resp);
     //showAlert("Product Successfully added");
     
  }); 
  request.error(function( msg ) {
    modal.hide();
    showAlert("Oops! check internet connection"); 
  });
}

function loginCustomer(username,password){
  modal.show();
  var request = $.ajax({
    url: API + 'loginCustomer/',
    type: "POST",
    timeout:20000,
    datatype:'json',
    data: {  
      username:username,
      password:password
    } 
  }); 
  request.done(function(resp) {
    modal.hide();
    localStorage.setItem('customerInfo',resp);
    console.log(resp);
    resp=jQuery.parseJSON(resp);
    if(resp.response!="Success"){
      showAlert(resp.response);
      return;
    }else{
      $("#menumerchant").click();
    }
    localStorage.setItem("merchantInfo",resp);
     //showAlert("Product Successfully added");
     
  }); 
  request.error(function( msg ) {
    modal.hide();
    showAlert("Oops! check internet connection"); 
  });
}

function showHomeMerchants(){
  tab='';
  for(i=0; i<merchants_db.length; i++){
     
    tab+=' <div class="col-md-4 col-sm-4 col-xs-6">'+
                    '<div class="merchant">'+
                        '<div class="tint"></div>'+
                        '<div class="content-block">'+
                            '<div class="row">'+
                                '<div class="col-xs-12 balls" data-merchant-icon="'+initials(merchants_db[i].companyname)+'">'+merchants_db[i].companyname+'</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>';
     
  }
  $(".homemenuu").append(tab);
}

function showAlert(msg){
  ons.notification.alert({ message: msg });
}

function initials(reporter){
  return reporter[0].toUpperCase();

  parts=reporter.split(" "); 
  if (parts.length == 1){
    return parts[0];
  }else{
    return parts[0][0]+parts[1][0];
  }
}