-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 17, 2015 at 05:58 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dynamicforms`
--
CREATE DATABASE IF NOT EXISTS `dynamicforms` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `dynamicforms`;

-- --------------------------------------------------------

--
-- Table structure for table `campaign`
--

CREATE TABLE IF NOT EXISTS `campaign` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `campaign_name` varchar(25) NOT NULL,
  `createdby` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `campaign`
--

INSERT INTO `campaign` (`id`, `campaign_name`, `createdby`) VALUES
(2, 'new campaign', 'moyin agbaje'),
(4, 'new campaign', 'moyin agbaje'),
(5, 'shola campaign', 'joshua'),
(6, 'cool campaign', 'joshua');

-- --------------------------------------------------------

--
-- Table structure for table `contactforms`
--

CREATE TABLE IF NOT EXISTS `contactforms` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `fieldid` varchar(25) NOT NULL,
  `value` varchar(25) NOT NULL,
  `contactid` int(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=85 ;

--
-- Dumping data for table `contactforms`
--

INSERT INTO `contactforms` (`id`, `fieldid`, `value`, `contactid`) VALUES
(17, '1', 'Joshua Ajayi', 3),
(18, '2', '7033556109', 3),
(19, '3', 'male', 3),
(20, '4', 'email', 3),
(21, '1', 'Joshua Ajayi', 4),
(22, '2', '7033556109', 4),
(23, '3', 'male', 4),
(24, '4', 'email', 4),
(25, '1', 'Joshua Ajayi', 5),
(26, '2', '7033556109', 5),
(27, '3', 'male', 5),
(28, '4', 'email', 5),
(29, '1', 'Moyin Agbaje', 6),
(30, '2', '7030253949', 6),
(31, '3', 'female', 6),
(32, '4', 'tinkerbellmyn@yahoo.com', 6),
(33, '1', 'Joshua Ajayi', 7),
(34, '2', '7033556109', 7),
(35, '3', 'male', 7),
(36, '4', 'email', 7),
(37, '1', 'Moyin Agbaje', 8),
(38, '2', '7030253949', 8),
(39, '3', 'female', 8),
(40, '4', 'tinkerbellmyn@yahoo.com', 8),
(41, '1', 'Joshua Ajayi', 9),
(42, '2', '7033556109', 9),
(43, '3', 'male', 9),
(44, '4', 'email', 9),
(45, '1', 'Moyin Agbaje', 10),
(46, '2', '7030253949', 10),
(47, '3', 'female', 10),
(48, '4', 'tinkerbellmyn@yahoo.com', 10),
(49, '1', 'Joshua Ajayi', 11),
(50, '2', '7033556109', 11),
(51, '3', 'male', 11),
(52, '4', 'email', 11),
(53, '1', 'Moyin Agbaje', 12),
(54, '2', '7030253949', 12),
(55, '3', 'female', 12),
(56, '4', 'tinkerbellmyn@yahoo.com', 12),
(57, '1', 'Joshua Ajayi', 13),
(58, '2', '7033556109', 13),
(59, '3', 'male', 13),
(60, '4', 'email', 13),
(61, '1', 'Moyin Agbaje', 14),
(62, '2', '7030253949', 14),
(63, '3', 'female', 14),
(64, '4', 'tinkerbellmyn@yahoo.com', 14),
(65, '1', 'femi obadina', 15),
(66, '2', '8023970193', 15),
(67, '3', 'male', 15),
(68, '4', 'veiledhero@yahoo.com', 15),
(69, '1', 'hjjjhj', 18),
(70, '2', '666', 18),
(71, '3', 'male', 18),
(72, '8', 'jjhjh', 18),
(73, '1', 'hjjjhj', 19),
(74, '2', '999', 19),
(75, '3', 'male', 19),
(76, '8', 'jjhjh', 19),
(77, '1', 'hjjjhj', 20),
(78, '2', '99955', 20),
(79, '3', 'male', 20),
(80, '8', 'jjhjh', 20),
(81, '1', 'hjjjhj', 21),
(82, '2', '6677', 21),
(83, '3', 'male', 21),
(84, '8', 'jjhjh', 21);

-- --------------------------------------------------------

--
-- Table structure for table `contactlist`
--

CREATE TABLE IF NOT EXISTS `contactlist` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `phoneno` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `contactlist`
--

INSERT INTO `contactlist` (`id`, `phoneno`) VALUES
(23, '6677'),
(24, '7033556109'),
(25, '7030253949'),
(26, '8023970193');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `phoneno` varchar(25) NOT NULL,
  `contactname` varchar(25) NOT NULL,
  `gender` varchar(25) NOT NULL,
  `state` varchar(25) DEFAULT NULL,
  `age` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `phoneno`, `contactname`, `gender`, `state`, `age`) VALUES
(12, '6677', 'hjjjhj', 'male', 'jjhjh', NULL),
(13, '7033556109', 'joshua ajayi', 'male', 'lagos', NULL),
(14, '7030253949', 'moyin agbaje', 'female', 'akure', NULL),
(15, '8023970193', 'femi obadina', 'male', 'ibadan', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `filter`
--

CREATE TABLE IF NOT EXISTS `filter` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `campaignid` int(25) NOT NULL,
  `field` varchar(25) NOT NULL,
  `value` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `filter`
--

INSERT INTO `filter` (`id`, `campaignid`, `field`, `value`) VALUES
(2, 4, '3', 'female'),
(4, 5, '3', 'female'),
(15, 6, '2', '8023970193'),
(16, 2, '1', 'femi obadina');

-- --------------------------------------------------------

--
-- Table structure for table `formfields`
--

CREATE TABLE IF NOT EXISTS `formfields` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `fieldname` varchar(25) NOT NULL,
  `fieldkey` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `formfields`
--

INSERT INTO `formfields` (`id`, `fieldname`, `fieldkey`) VALUES
(1, 'contactname', NULL),
(2, 'phoneno', NULL),
(3, 'gender', NULL),
(8, 'state', NULL),
(9, 'age', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE latin1_general_ci NOT NULL,
  `status` varchar(200) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `status`) VALUES
(1, 'Super', '1'),
(2, 'Admin', '1');

-- --------------------------------------------------------

--
-- Table structure for table `role_assignments`
--

CREATE TABLE IF NOT EXISTS `role_assignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleID` varchar(200) COLLATE latin1_general_ci NOT NULL,
  `sys_modID` varchar(200) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `role_assignments`
--

INSERT INTO `role_assignments` (`id`, `roleID`, `sys_modID`) VALUES
(4, '2', '1'),
(5, '2', '2'),
(6, '2', '3'),
(7, '2', '4'),
(8, '2', '5'),
(9, '1', '1'),
(10, '1', '2'),
(11, '1', '3'),
(12, '1', '4'),
(13, '1', '5');

-- --------------------------------------------------------

--
-- Table structure for table `sys_mod`
--

CREATE TABLE IF NOT EXISTS `sys_mod` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `controller_name` varchar(200) COLLATE latin1_general_ci NOT NULL,
  `description` varchar(200) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `sys_mod`
--

INSERT INTO `sys_mod` (`id`, `controller_name`, `description`) VALUES
(1, 'forms::deleteCampaign', 'Delete Campaign'),
(2, 'forms::showCampaign', 'Show Campaign'),
(3, 'forms::campaignContacts', 'Campaign Contacts'),
(4, 'forms::deleteFilter', 'Delete Filter'),
(5, 'forms::createfilter', 'Create Filter'),
(6, 'forms::createform', 'Create Form'),
(7, 'forms::updateSettings', 'Update Settings'),
(8, 'forms::createcampaign', 'Create Campaign'),
(9, 'forms::editcampaign', 'Edit Campaign'),
(10, 'forms::uploadContact', 'Upload Contact');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `usertype` varchar(255) NOT NULL,
  `email` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `usertype`, `email`) VALUES
(3, 'joshua', '8ee30677a3cfe34ae8e24d4b9ac405dc', 'Superadmin', 'jayboie101@gmail.com'),
(6, 'iolufemi', '2ccb26b6b7b4ed3eb8fb73f522cf95cc', 'Superadmin', ''),
(12, 'deji', 'f94adcc3ddda04a8f34928d862f404b4', 'Admin', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;